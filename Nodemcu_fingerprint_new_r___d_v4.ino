#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>
#include <SPI.h>
#include <SD.h>
#include <MyRealTimeClock.h>
WiFiClient client;

MyRealTimeClock myRTC(D1, D2, D3);                ///// Assign Digital Pins for RTC
SoftwareSerial s(2,3);                           ////// TX ,RX for Serial Communication //////
File myFile;
HTTPClient http;
const char* host = "www.google.com";            
const int chipSelect = D8;
int sd_card_state = 0;
int post_data_state = 0;
int sd_card_empty = 1;
int punches_exhausted_in_sd_card = 0;
int i = 0;
int j = 0;
int count_of_punches_in_sd_card = 0;
String fingerprint_records;
String dataFromArduino;
String dataFromAll;
int string_data_from_arduino[3000];
char sd_card_data[3000];
String response;
int count_for_connection = 0;
int count_after_internet_connected = 0;
int daily_data_post_time = 23;                    // Time in 24 hour format

void setup() {
//  WiFi.begin("Raj", "raj@00321");
//  WiFi.begin("JioFi3_075187", "15x5y75vbc");
// WiFi.begin("OSWAL TOWER", "oswal@321");
//  WiFi.begin("Realme", "0987654321");
  Serial.begin(9600);                                
  s.begin(9600);           // Baud rate of Serial Communication
  pinMode(D0,OUTPUT);
  digitalWrite(D0,LOW);
//  myRTC.setDS1302Time(20, 30, 15, 6, 23, 12, 2019);
  /* To set the current time and date in specific format
  | Second 00 | Minute 59 | Hour 10 | Day 12 |  Month 07 | Year 2015 |
  */
}

void loop() {
  myRTC.updateTime();
//  Serial.print(myRTC.hours);
//  Serial.print(":");
//  Serial.print(myRTC.minutes);
//  Serial.print(":");
//  Serial.println(myRTC.seconds);
//  myFile = SD.open("sensor.txt", FILE_READ);  

if (WiFi.status() != WL_CONNECTED){
   count_for_connection =  count_for_connection + 1;
   Serial.println(count_for_connection);
  }

while(count_for_connection==1000){
  count_for_connection=0;
//  WiFi.begin("OSWAL TOWER", "oswal@321");
  WiFi.begin("JioFi3_075187", "15x5y75vbc");
//  WiFi.begin("Realme", "0987654321");
//WiFi.begin("Raj", "raj@00321");
  count_for_connection=0;
  }
  if (s.available() > 0){
    count_after_internet_connected = 0;
    fingerData();                         // Calling Fingerdata method
  }
  else{
    if (count_after_internet_connected == 2000){
      count_after_internet_connected = 0;
      if (client.connect(host, 80)){                    // For checking if internet is available 
        count_for_connection = 0;  
        if ((myRTC.hours) == daily_data_post_time){    // Time of dailydata posting
          myFile = SD.open("dailydata.txt", FILE_READ); //Opening & reading dailydata file from sd card
          while (myFile.available()){
            sd_card_data[j] = myFile.read();          
            dataFromAll+= String(sd_card_data[j]);   
            if (String(sd_card_data[j]) == "?"){     
              count_of_punches_in_sd_card+= 1;      
              if (count_of_punches_in_sd_card == 10){  
                punches_exhausted_in_sd_card = 1;
                if (sd_card_state == 0){
                  post_data_state = 1;
                  postData();
                  count_of_punches_in_sd_card = 0;    
                }
              }          
            }
          }
          if (sd_card_state == 0){
            post_data_state = 1;
            punches_exhausted_in_sd_card = 0;
            postData();                         
            count_of_punches_in_sd_card = 0;
            if (response == "done"){
              deleteData();                   
              sd_card_empty = 0;
            }
            sd_card_state = 2;
          }
          myFile.close();
        }
        else{
          myFile = SD.open("sensor.txt", FILE_READ);        //////////// FILE OPEN, READ AND DELETE  /////////////
          while (myFile.available()) {
            sd_card_data[j] = myFile.read();              // Storing sd card data to character array
            dataFromAll+= String(sd_card_data[j]);       // Concatenate charcter array and converting to string array
            if (String(sd_card_data[j]) == "?"){        // For counting the no. of punches
              count_of_punches_in_sd_card+= 1;
              if (count_of_punches_in_sd_card == 10){ // For posting 10 datas at a time
                punches_exhausted_in_sd_card = 1;    // Remains 1 until every punches in sd card has been posted
                if (sd_card_state == 1){
                  post_data_state = 1;             // For posting data and becomes 0 after posting
                  postData();                     // Calling postdata method
                  count_of_punches_in_sd_card = 0;    
                }
              }          
            }
          }
          if(sd_card_state == 1){
            post_data_state = 1;
            punches_exhausted_in_sd_card = 0;
            postData();                             // Calling postdata for live punching
            count_of_punches_in_sd_card = 0;
            if(response == "done"){
              deleteData();                       // Calling deletedata method
              sd_card_empty = 0;
            }
            sd_card_state = 0;
          }
          myFile.close();
        }
      }
    }
    else{
      count_after_internet_connected = count_after_internet_connected + 1;
      Serial.println(count_after_internet_connected);
    }
  }
}
  

void fingerData(){
  while (s.available() > 0){
    sd_card_empty = 1;                                            // Initialized as 1 and becomes 0 after posting all data
    string_data_from_arduino[i] = s.read();
    dataFromArduino = String(string_data_from_arduino[i]);      // Converting string array to string
    sd_card_state = 1;                                         // For posting data and becomes 0 after posting
    sdCard();                                                 // Calling sdcard method
  }  
}


void postData(){
  fingerprint_records = dataFromAll;
  Serial.println(fingerprint_records);
 
  if (post_data_state == 1){
    http.begin("http://waytohr.herokuapp.com/transaction/fingerprint_records_entry?box_id=1&fingerprint_records=" +fingerprint_records);   //Specify request destination
    http.addHeader("Content-Type", "text/plain");  //Specify content-type header
    int httpCode = http.POST("fingerprint_records");   //Send the request
    String payload = http.getString();   //Get the response payload
    response = payload;
    Serial.println(response);
    dataFromAll="";
    post_data_state = 0;
    if (punches_exhausted_in_sd_card == 0){
      sd_card_state = 0;
    }
  }
  http.end();
}

void sdCard(){
  if (!SD.begin(chipSelect)){
    Serial.println("Initialization failed!");
    return;
  }
  myFile = SD.open("sensor.txt", FILE_WRITE);     // FILE_WRITE opens file for writing and moves to the end of the file, returns 0 if not available
  if (myFile) {
    myFile.print(myRTC.year); myFile.print("-"); myFile.print(myRTC.month); myFile.print("-"); myFile.print(myRTC.dayofmonth);
    myFile.print("/");  myFile.print(myRTC.hours); myFile.print(":"); myFile.print(myRTC.minutes); myFile.print(":"); myFile.print(myRTC.seconds);
    myFile.print("/"); myFile.print(dataFromArduino); myFile.print("?");
  }
  myFile = SD.open("dailydata.txt", FILE_WRITE);
  if (myFile) {
    myFile.print(myRTC.year); myFile.print("-"); myFile.print(myRTC.month); myFile.print("-"); myFile.print(myRTC.dayofmonth);
    myFile.print("/");  myFile.print(myRTC.hours); myFile.print(":"); myFile.print(myRTC.minutes); myFile.print(":"); myFile.print(myRTC.seconds);
    myFile.print("/"); myFile.print(dataFromArduino); myFile.print("?");
  }
  myFile = SD.open("alldata.txt", FILE_WRITE);
  if (myFile) {
    myFile.print(myRTC.year); myFile.print("-"); myFile.print(myRTC.month); myFile.print("-"); myFile.print(myRTC.dayofmonth);
    myFile.print("/");  myFile.print(myRTC.hours); myFile.print(":"); myFile.print(myRTC.minutes); myFile.print(":"); myFile.print(myRTC.seconds);
    myFile.print("/"); myFile.print(dataFromArduino); myFile.print("?");
  }
  digitalWrite(D0,HIGH);                 ////////////////////////////FOR TONE AFTER WRITING TO SD CARD////////////////////////
  delay(1000);
  digitalWrite(D0,LOW);
  myFile.close();
}

void deleteData(){       
  if((myRTC.hours) == daily_data_post_time){               // For deleting dailydata
    if (SD.exists("dailydata.txt")) {
      if (SD.remove("dailydata.txt")) Serial.println("File successfully deleted"); else Serial.println("Error - file not deleted");
    }
  }
  else{
    if (SD.exists("sensor.txt")){                      // For deleting live data
      if (SD.remove("sensor.txt")) Serial.println("File successfully deleted"); else Serial.println("Error - file not deleted");
    }
  }
}
